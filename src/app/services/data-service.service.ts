import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Person } from '../Model/person';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})


export class DataServiceService {

  url = environment.url;

  constructor(private http: HttpClient) { }

  loadPerson(): Observable<Person[]>{
    return this.http.get<Person[]>(this.url);
  }

  findPerson(id: number): Observable<Person>{
    return this.http.get<Person>(this.url + '/' + id);
  }

  agregarPersona(person: Person){
    return this.http.post(this.url, person);
  }

  modificarPersona(idPerson: number, person: Person){
    let url: string;
    url = this.url + '/' + idPerson;
    this.http.put(url, person)
      .subscribe(
        (response) => {
          console.log('resultado modificar persona' + response.toString);
        },
        (error) => console.log('error en modificar persona ' + error)
    );
  }

  eliminarPersona(idPersona: number){
    let url: string;
    url = this.url + '/' + idPersona;
    this.http.delete(url)
      .subscribe(
        (response) => {
          console.log('resultado modificar persona ' + response);
        },
        (error) => {
          console.log('error en modificar persona ' + error);
        }
    );
  }
}
