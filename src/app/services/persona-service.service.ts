import { DataServiceService } from './data-service.service';
import { Injectable } from '@angular/core';
import { Person } from '../Model/person';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonaServiceService {

  persons: Person[] = [];

  constructor(private dataService: DataServiceService){
  }

  setPersonas(persons: Person[]): void{
    this.persons = persons;
  }

  obtenerpersonas(): Observable<Person[]>{
    return this.dataService.loadPerson();
  }

  agregarPersona(person: Person): void{
    console.log('persona a agregar' + person.name);
    this.dataService.agregarPersona(person)
      .subscribe(
        (person: Person) =>{
          console.log('se agrega al areglo la persona recien insertada suscriber: ' + person.userId);
          this.persons.push(person);
        });
  }

  encontrarPersona(idPersona: number): Observable<Person>{
    return this.dataService.findPerson(idPersona);
  }

  modificarPersona(id: number, persona: Person): void{
    console.log('persona a modificar' + persona.userId);
    console.log('desdes el service');
    console.log(persona);
    this.dataService.modificarPersona(id, persona);
  }

  eliminarPersona(id: number): void{
    console.log('eliminar persona con id: ' + id);
    const index = this.persons.findIndex(persona => persona.userId === id);
    this.persons.splice(index, 1);
    this.dataService.eliminarPersona(id);
  }
}

