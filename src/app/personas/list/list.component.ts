import { Component, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Person } from 'src/app/Model/person';
import { PersonaServiceService } from 'src/app/services/persona-service.service';

import { MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  displayedColumns: string[] = ['ID', 'Name', 'DocType', 'docnum', 'email', 'tel', 'action'];
  dataSource: MatTableDataSource<Person>;

  personas: Person[] = [];

  constructor(private personaService: PersonaServiceService, private router: Router, private route: ActivatedRoute ) {}

  ngOnInit(): void {
    this.personaService.obtenerpersonas()
      .subscribe(
        (personasObtenidas: Person[]) => {
          this.personas = personasObtenidas;
          this.personaService.setPersonas(this.personas);
          this.dataSource = new MatTableDataSource(personasObtenidas);
        }
      );
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  goUpdate(row: any): void{
    console.log(row);
    this.router.navigateByUrl(`update/${row.userId}`);
  }

  onDelete(id: number): void{
    alert('seguro quiere eliminar');
    console.log(id);
    if (id != null){
      console.log('Persona a eliminar ' + id);
      this.personaService.eliminarPersona(id);
    }
    this.dataSource = new MatTableDataSource(this.personas);
  }

  goCreate(): void{
    this.router.navigateByUrl('update/');
  }
}
