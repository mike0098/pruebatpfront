import { Person } from 'src/app/Model/person';
import { PersonaServiceService } from './../../services/persona-service.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-update',
  templateUrl: './form-update.component.html',
  styleUrls: ['./form-update.component.scss']
})

export class FormUpdateComponent implements OnInit {
  person: Person;
  id: number;
  tittle: string;
  name: string;
  doctype: string;
  docNum: string;
  email: string;
  tel: string;
  constructor(private personaService: PersonaServiceService, private route: ActivatedRoute, private router: Router,
              private formBuilder: FormBuilder) { }

  registerForm = this.formBuilder.group({});

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;

    this.personaService.encontrarPersona(this.id)
      .subscribe(
        (personaObtenida: Person) => {
          this.person = personaObtenida;
          this.name = this.person.name;
          this.doctype = this.person.doctype;
          this.docNum = this.person.docNum;
          this.email = this.person.email;
          this.tel = this.person.tel;
          if (!this.id){
            this.tittle = 'Create';
          }else{
            this.tittle = 'Update';
          }
        }
      );
    this.registerForm = this.formBuilder.group({
      id: [this.id, Validators.required],
      name: ['', Validators.required],
      doctype: ['cedula', Validators.required],
      docNum: ['', Validators.required],
      email: ['', Validators.required],
      tel: ['', Validators.required]
    });
    console.log(this.id);
  }

  action(): void{
    console.log(this.registerForm.valid);
    console.log(this.id);
    const person: Person = new Person(this.id, this.name, this.doctype, this.docNum, this.tel, this.email);
    if (!this.id){
      person.userId = null;
      this.personaService.agregarPersona(person);
    }else{
      this.personaService.modificarPersona(this.id, person);
    }
    setTimeout(() => {
        this.router.navigate(['home']);
      },
      1000);
  }

  back(): void{
    this.router.navigateByUrl('home');
  }
}
