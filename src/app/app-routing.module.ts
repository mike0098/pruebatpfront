import { FormUpdateComponent } from './personas/form-update/form-update.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './personas/list/list.component';

const routes: Routes = [
  {path: 'home', component: ListComponent},
  {path: 'update/:id', component: FormUpdateComponent},
  {path: 'update', component: FormUpdateComponent},
  {path: '', pathMatch: 'full', redirectTo: 'home'},
  {path: '**', pathMatch: 'full', redirectTo: 'home'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
