export class Person {
  constructor(public userId: number, public name: string, public doctype: string,
              public  docNum: string, public tel: string, public email: string){}
}
