import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DataServiceService } from './services/data-service.service';
import { PersonaServiceService } from './services/persona-service.service';
import { ListComponent } from './personas/list/list.component';
import { FormUpdateComponent } from './personas/form-update/form-update.component';
import { HttpClientModule } from '@angular/common/http';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/shared/material/material.module';
import { MatSortModule } from '@angular/material/sort';
import { HeaderComponent } from './personas/header/header.component';


@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    FormUpdateComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MaterialModule,
    MatSortModule,
    ReactiveFormsModule
  ],
  providers: [
    DataServiceService,
    PersonaServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
